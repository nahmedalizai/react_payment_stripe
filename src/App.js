import React, { useState } from 'react';
import './App.css';
import ProductList from './components/ProductList'
import Cart from './components/Cart'
import data from './data/Products.json'

function App() {
  const [itemList] = useState(data)
  const [cartItems, setCartItems] = useState([])
  var selectedItem = null

  const addItemToCart = (item) => {
    selectedItem = cartItems.find(i => i.id === item.id)
    if (selectedItem !== undefined && selectedItem !== null) { // TODO : change this logic to update the existing item in the card
      setCartItems(cartItems => cartItems.filter(i => i.id !== item.id));
      setCartItems(cartItems => [
        ...cartItems,
        {
          id: item.id,
          name: item.name,
          description: item.description,
          price: item.price,
          quantity: selectedItem.quantity + 1
        }
      ])
    }
    else {
      setCartItems(cartItems => [
        ...cartItems,
        {
          id: item.id,
          name: item.name,
          description: item.description,
          price: item.price,
          quantity: 1
        }
      ])
    }
  }

  const removeItemFromCart = (itemId) => {
    setCartItems(cartItems => cartItems.filter(item => item.id !== itemId))
  }

  const emptyCart = () => {
    setCartItems([])
  }
  return (
    <div className="App">
      <div style={{ width: "58%", float: "left", margin: "3px" }}>
        <ProductList data={itemList} addItem={addItemToCart} />
      </div>
      <div style={{ width: "40%", float: "right", margin: "3px" }}>
        <Cart data={cartItems} removeItem={removeItemFromCart} setDataEmpty={emptyCart}/>
      </div>
    </div>
  );
}

export default App;
