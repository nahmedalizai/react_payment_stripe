import React, { useState } from 'react'
import { Button, Table, Pane } from 'evergreen-ui'

function ProductList(props) {
    const [itemList] = useState(props.data !== undefined && props.data !== [] ? props.data : [])
    //const [selectedItem, setSelectedItem] = useState()

    return (
        <div>
            <Pane elevation={1}>
                <Table>
                    <Table.Head>
                        <Table.TextHeaderCell flexBasis={150} flexShrink={0} flexGrow={0}>
                            Product
                    </Table.TextHeaderCell>
                        <Table.TextHeaderCell>
                            Description
                    </Table.TextHeaderCell>
                        <Table.TextHeaderCell flexBasis={50} flexShrink={0} flexGrow={0}>
                            Price
                    </Table.TextHeaderCell>
                        <Table.TextHeaderCell flexBasis={120} flexShrink={0} flexGrow={0} />
                    </Table.Head>
                    <Table.Body>
                        {itemList.map(item => (
                            <Table.Row key={item.id}>
                                <Table.TextCell flexBasis={150} flexShrink={0} flexGrow={0}>{item.name}</Table.TextCell>
                                <Table.TextCell>{item.description}</Table.TextCell>
                                <Table.TextCell flexBasis={50} flexShrink={0} flexGrow={0}>{item.price}</Table.TextCell>
                                <Table.TextCell flexBasis={120} flexShrink={0} flexGrow={0}>
                                    <Button appearance="minimal" intent="success" onClick={() => props.addItem(item)}>Add to cart</Button>
                                </Table.TextCell>
                            </Table.Row>
                        ))}
                    </Table.Body>
                </Table>
            </Pane>
        </div>
    )
}

export default ProductList
