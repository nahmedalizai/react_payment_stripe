import React, { useState, useEffect } from 'react'
import { Button, Table, Pane, Text } from 'evergreen-ui'
import Checkout from './Checkout'
// import axios from 'axios'
// import { StripeProvider, Elements } from 'react-stripe-elements'

function Cart(props) {

    const [itemList, setItemList] = useState(props.data !== undefined && props.data.length > 0 ? props.data : [])
    const [isEmpty, setIsEmpty] = useState(true)

    useEffect(() => {
        console.log('calls')
        if (props.data !== undefined && props.data.length > 0) {
            setIsEmpty(false)
        }
        else {
            setIsEmpty(true)
        }
        setItemList(
            props.data
        )
    }, [props.data]);

    return (
        <div>
            <Pane elevation={1}>
                <Table>
                    <Table.Head>
                        <Table.TextHeaderCell style={{ textAlign: "center" }}>
                            Cart
                    </Table.TextHeaderCell>
                    </Table.Head>
                    <Table.Body>
                        <Table.TextCell>
                            <Pane clearfix>
                                {
                                    itemList.map(
                                        item => (
                                            <Pane
                                                elevation={4}
                                                float="left"
                                                width={200}
                                                height={120}
                                                margin={24}
                                                display="flex"
                                                justifyContent="center"
                                                alignItems="center"
                                                flexDirection="column"
                                                key={item.id}
                                            >
                                                <Text>{item.name}</Text>
                                                <Text size={300}>Quantity : {item.quantity}</Text>
                                                <br />
                                                <Button appearance="minimal" intent="danger" onClick={() => props.removeItem(item.id)}>Remove</Button>
                                            </Pane>
                                        )
                                    )
                                }
                            </Pane>
                        </Table.TextCell>
                        <Table.TextCell style={{ textAlign: "center" }}>
                            {isEmpty ? "Cart is empty" : <Checkout  selectedProducts={itemList} isSuccess={props.setDataEmpty}/>}
                        </Table.TextCell>
                    </Table.Body>
                </Table>
            </Pane>
        </div>
    )
}

export default Cart
