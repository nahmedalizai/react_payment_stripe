import React, { useState } from 'react'
import { Button, Pane, Dialog } from 'evergreen-ui'
import axios from 'axios'
import { PushSpinner } from "react-spinners-kit";

import {
    CardNumberElement,
    CardExpiryElement,
    CardCVCElement,
    injectStripe
} from 'react-stripe-elements'

function CheckoutForm({ stripe, selectedProducts, isSuccess }) {
    const [isShown, setIsShown] = useState(false)
    const [receiptUrl, setReceiptUrl] = useState('')
    const [paymentSuccess, setPaymentSuccess] = useState(false)
    const [isLoading, setIsLoading] = useState(false)

    const handleSubmit = async event => {
        setIsLoading(true)
        event.preventDefault()
        //const { token } = await stripe.createToken()

        await axios.post('http://localhost:7000/api/stripe/charge', {
            amount: '999',//selectedProduct.price.toString().replace('.', ''),
            currency: 'eur',
            source: 'tok_mastercard',//token.id,
            receipt_email: 'customer@example.com'
        }).then(response => {
            setReceiptUrl(response.data.charge.receipt_url)
            setPaymentSuccess(true)
            setIsLoading(false)
        }).catch(error => {
            console.log(error.response.data.message)
            setIsLoading(false)
        })

    }

    const closeDialog = () => {
        isSuccess(true)
        setIsShown(false)
    }

    const success = (<div className="success">
        <h2>Payment Successful!</h2>
        <a href={receiptUrl} target="_blank">View Receipt</a>
    </div>)
    return (
        <div>
            <Pane>
                <Dialog
                    isShown={isShown}
                    title="Stripe Purchase"
                    onCloseComplete={closeDialog}
                    //confirmLabel="Custom Label"
                    hasFooter={false}
                >
                    <div style={{marginLeft:"35%"}}>
                        <PushSpinner size={30} color="#14B5D0" loading={isLoading} />
                    </div>
                    {paymentSuccess ? success :
                        <form onSubmit={handleSubmit}>
                            <label>
                                Card details
                            <CardNumberElement />
                            </label>
                            <label>
                                Expiration date
                            <CardExpiryElement />
                            </label>
                            <label>
                                CVC
                            <CardCVCElement />
                            </label>

                            <Button type="submit" appearance="primary" className="order-button"> Pay </Button>
                        </form>}
                </Dialog>
                <Button style={{ margin: "5px" }} appearance="primary" onClick={() => setIsShown(true)}>Confirm Purchase</Button>
            </Pane>
        </div>
    )
}

export default injectStripe(CheckoutForm)
