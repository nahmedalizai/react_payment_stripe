import React, { useState } from 'react'
import { StripeProvider, Elements } from 'react-stripe-elements'
import CheckoutForm from './CheckoutForm'

function Checkout(props) {
    const [state, setState] = useState({
        stripe: null
    })

    document.querySelector('#stripe-js').addEventListener('load', () => {
        // Create Stripe instance once Stripe.js loads
        setState({ stripe: window.Stripe('pk_test_R4IfJaoAFQJ8mPuDYkrupE1800x6uJIKHP') });
    });

    return (
        <div>
            <StripeProvider stripe={state.stripe}>
                <Elements>
                    <CheckoutForm selectedProducts={props.selectedProducts} isSuccess={props.isSuccess}/>
                </Elements>
            </StripeProvider>

        </div>
    )
}

export default Checkout
